UiTextEdit_t* edit = NULL;
UiTextEdit_t* range1 = NULL;
UiTextEdit_t* range2 = NULL;
UiMainForm_t* mainform = NULL;

int main()
{
    int failed = app_sysinit();
    if(failed)
    {
        printf("System failed to initialize \n");
        return failed;
    }
    printf("System Initialized\n");
    //write your code after this line
    UiControlInfo_t info = {0};

    info.parent = NULL;

    info.typeId = enControlTypeIdValue_Mainform;

    info.location.X = 10;
    info.location.Y = 10;
    info.size.Height = 800;
    info.size.Width = 1200;
    info.maximumSize.Height = 800;
    info.maximumSize.Width  = 1600;
    info.minimumSize.Height = 600;
    info.minimumSize.Width  = 600;
    info.text = "Test MainForm";

    pcf_status_t status;

    UiMainForm_t* mainform = pcf_ui_mainform_create(&info, &status);

    if(mainform){
        int32_t exitcode;
		addLabelrange1(mainform, 10, 70, 100, 25, "This is a label.");
		addLabelrange2(mainform, 10, 90, 100, 25, "This is a label.");
		addLabelmain(mainform, 10, 30, 150, 35, "This is a label.");

        addTextEdit(mainform, 180, 30, 150, 35, "Editor");
		addTextEditrange1(mainform, 120, 70, 100, 25, "Editor");
		addTextEditrange2(mainform, 120, 90, 100, 25, "Editor");
        addButton(mainform, 10, 120, 50, 50, "Button");
		//testGraphicsContext(mainform, 350, 30, 700, 700);

        status = pcf_ui_mainform_run(mainform, &exitcode);//

        fprintf(stdout, "mainform exited with code = %d, status = %d\n", exitcode, status);
    }

    return 0;
}

void addLabelmain(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(mainform);
    if(parent)
    {
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_Label;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = "Enter Expression";//Shall have text "What?"
        pcf_status_t status;



        UiLabel_t* label = pcf_ui_label_new(&info,&status);

        if(status)
        {
            fprintf(stdout, "Failed while creating label control, status = %d\n", status);
            return;
        }
        else
        {
            status = pcf_ui_label_setTextColor(label, enGraphicsColorCode_Black);

            if(status)
            {
                fprintf(stdout, "Failed while setting color for label control, status = %d\n", status);
            }
            status = pcf_ui_label_setBackgroundColor(label, enGraphicsColorCode_White);
        }
    }
}

void PCF_STDCALL buttonClickHandler(UiButton_t* button)
{
    UiControl_t* control = pcf_ui_button_asControl(button);
    UiControl_t* textControl = pcf_ui_textedit_asControl(edit);
	UiControl_t* textControlrange1 = pcf_ui_textedit_asControl(range1);
	UiControl_t* textControlrange2 = pcf_ui_textedit_asControl(range2);
    pcf_ui_property_text text;
    pcf_ui_property_text textEditText;
	
	
    
    testGraphicsContext(mainform, 350, 30, 700, 700);
    
	

    if(!pcf_ui_control_getText(control, text))
    {
        fprintf(stdout, "Button with text = %s is clicked\n", text);
    }

    if(!pcf_ui_control_getText(textControl, textEditText))
    {
        fprintf(stdout, "Text in text Edit box is %s\n", textEditText);
    }
	
	if(!pcf_ui_control_getText(textControlrange1, textEditText))
    {
        fprintf(stdout, "Text in text Edit box range 1 is %s\n", textEditText);
    }
	if(!pcf_ui_control_getText(textControlrange2, textEditText))
    {
        fprintf(stdout, "Text in text Edit box range 2 is %s\n", textEditText);
    }
	
}