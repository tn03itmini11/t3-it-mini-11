void testGraphicsContext(UiMainForm_t* parentForm, int x, int y, int width, int height)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(parentForm);
    if(parent)
    {
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_DrawingView;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = "This is a dview";//Shall have text "What?"
        pcf_status_t status;
        UiDrawingView_t* view = pcf_ui_drawingView_new(&info,&status);
        if(view)
        {
            fprintf(stdout, "Created drawing view\n");
            status = pcf_ui_drawingView_attachPaintEventHandler(view, DrawingViewPaintEventHandler);
            if(status)
            {
                fprintf(stdout, "Failed while adding paint handler to drawing-view\n");
            }
            else
            {
                UiControl_t* drawingControl = pcf_ui_drawingView_asControl(view);
                pcf_ui_control_attachMouseClickEventHandler(drawingControl, DrawingViewMouseClickEventHandler);
                pcf_ui_control_attachMouseMoveEventHandler(drawingControl, DrawingViewMouseMoveEventHandler);
                fprintf(stdout, "Successfully added paint handler to drawing-view\n");
            }
        }
        else
        {
            fprintf(stdout, "Failed while creating drawing view\n");
        }
    }
    else
    {
        fprintf(stdout, "Failed while converting parent as control\n");
    }
}
